//This is a root Component which is rendered whenever Application open
import React from "react"
import Login from "./src/screens/auth/Login"

export default App = () => <Login />